这是一个邮件服务，用来在自动构建成功以后发送电子邮件给选择的人。

## 启动

```bash
$ npm start
```

## 测试

### 测试 logger

使用 postman 访问 `http://localhost:28225/logger`。
